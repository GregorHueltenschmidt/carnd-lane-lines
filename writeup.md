# **Finding Lane Lines on the Road** 

## Writeup 

**Finding Lane Lines on the Road**

The goals / steps of this project are the following:
* Make a pipeline that finds lane lines on the road
* Reflect on your work in a written report

---

### Reflection

#### 1. Describe your pipeline. As part of the description, explain how you modified the draw_lines() function.

My preprocessing pipeline consisted of 5 steps. First, I converted the images to grayscale, then I applied Gaussian blurring to the grayscale image. Then I detected edges by means of the canny edge detection algorithm. After masking the detected images with an appropiate region of interest (in this case a quadrilateral polygon) I applied hough transform to the masked edges in order to extract line segments. All these steps were performed with the opencv library. In the next step the extracted line segments were drawn onto the original image. In order to draw a single line on the left and right, I had to average and extrapolate these line segments. Due to their slope i divided the segments into two groups, left and right lines. I excluded those with an absolute value smaller than 0.5, because these line segments were obviously no lane lines. I averaged the line segments in the two groups by a simple linear regression with the numpy polyfitfunction. In this way I received the two equations for the left and the right lane. In the last step I drew the two lanes onto the original image inside the defined region of interest.

#### 2. Identify potential shortcomings with your current pipeline


One potential shortcoming would be what would happen when the contrast in the grayscale image between the lane line and the lane itself is too weak. Another shortcoming could be the error caused by outliers.


#### 3. Suggest possible improvements to your pipeline

A possible improvement would be to transform into another color space such as hue. Another potential improvement could be a better handling of outliers and/or applying a higher order polynomial function in the regression step.

## Video
The algorithm was applied to two video sequences.
![Finding white lane lines](test_videos_output/solidWhiteRight.mp4)

![Finding yellow lane lines](test_videos_output/solidYellowLeft.mp4)


